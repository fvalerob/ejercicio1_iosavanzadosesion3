//
//  ViewController.swift
//  Ejercicio1_IosAvanzadoSesion3
//
//  Created by Fran on 17/11/17.
//  Copyright © 2017 Fran. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIPopoverPresentationControllerDelegate, TableViewControllerDelegate{

  
    @IBAction func accionOpciones(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "tablaStoryboard") as! TableViewController
        
        controller.delegate = self
        controller.preferredContentSize = CGSize(width: 250, height: 135)

        // Presentamos el controlador
        // - en iPad, será un Popover
        // - en iPhone, será un action sheet
        controller.modalPresentationStyle = UIModalPresentationStyle.popover
        
        self.present(controller, animated: true, completion: nil)
        
        // Configuramos el popover (sí, después de presentarlo, es tan ilógico como parece)
        let popController = controller.popoverPresentationController
        
        // Todo popover debe tener un botón de referencia o una posición en la pantalla desde la que sale (con barButtonItem o con sourceView):
        popController?.barButtonItem = self.navigationItem.rightBarButtonItem
        popController?.permittedArrowDirections = UIPopoverArrowDirection.any
        popController?.delegate = self
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func opcionSeleccionada(nombreOpcion: String) {
        // Comprobamos que se recibe aquí la opción seleccionada. Aquí ya podríamos hacer lo que queramos con el valor recibido.
        print("Seleccionado \(nombreOpcion)")
        
        // Ocultamos el popover
        self.dismiss(animated: true, completion: nil)
    }
}

